
function makeAjaxRequest(url, method, callback) {
    let xhr = new XMLHttpRequest();
    xhr.open(method, url, true);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            callback(JSON.parse(xhr.responseText));
        }
    };
    xhr.send();
}

makeAjaxRequest('https://ajax.test-danit.com/api/swapi/films', 'GET', function (filmsData) {
    
    filmsData.forEach(function (film) {
        console.log('Епізод: ' + film.episodeId);
        console.log('Назва: ' + film.name);
        console.log('Зміст: ' + film.openingCrawl);

        
        let charactersPromises = film.characters.map(function (characterUrl) {
            return new Promise(function (resolve, reject) {
                makeAjaxRequest(characterUrl, 'GET', function (characterData) {
                    resolve(characterData);
                });
            });
        });

        Promise.all(charactersPromises).then(function (charactersData) {
            console.log('Персонажі:');
            charactersData.forEach(function (character, index) {
                console.log((index + 1) + '. ' + character.name);
            });
        });
    });
});
